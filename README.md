# Supplementary Files to "Joint Variable Selection of both Fixed and Random Effects for Gaussian Process-based Spatially Varying Coefficient Models"

This repository contains all necessary files, data, and code to reproduce the results of the manuscript "Joint Variable Selection of both Fixed and Random Effects for Gaussian Process-based Spatially Varying Coefficient Models" by Dambon et al. (2021), arXiv: . 

## TeX

This directory contains the TeX files to build the manuscript. Run the main file. 

## Simulation

In the subdirectory scripts, the code for the simulation is given. If runs and evaluates the the simulation. Run 00_run_simulation.R. The subdirectory output contains all figures and tables of the manuscript. The subdirectory data contains the results of all simulations as well as the meta data of the simulations.

## Application

Similar to Simulation


## R-package

Contains the GNU zipped tar of the package varycoef with the implemented selection method. 
