\section{Application}\label{sec:real}

\subsection{Data}

As an application, we consider the Dublin Voter data set. It consists of the voter turnout in the 2002 General Elections (GE) and 8 other demographic covariates for $n = 322$ electoral divisions (ED) in the Greater Dublin area (Ireland), see Figure~\ref{fig:01_GenEl2004}. All nine  variables are given in percentages and we provide an overview in Table~\ref{tab:DubVoter_sumstat}. The data set was first studied by \citet{Kavanagh2004}. The data set is available in the \textsf{R} package \pkg{GWmodel}. The article by \citet{Gollini2015} showcases further analysis using the GWR framework. In particular, a GWR selection based on a corrected AIC \citep{Hurvich1998} is conducted. The goal of this section is to do variable selection using the PMLE. Further, we compare our results to linear model-based LASSO and an GWR-based selections.

\begin{figure}
	\centering
	\includegraphics[scale=1]{../application/output/fig_01.png}
	\caption{Voter turnout in the 2002 General Elections per ED. The black dots represent the observation locations per ED used in our analysis. They are provided in the data set, too.}\label{fig:01_GenEl2004}
\end{figure}


\input{../application/output/table_DubVoter_sumstat.tex}


As one can see in Table~\ref{tab:DubVoter_sumstat}, the summary statistics of the variables vary a lot, which can be an issue in numeric optimization. Therefore, without loss of generality and interpretability, we standardize the data by subtracting the empirical mean and scaling by the empirical standard deviation. We annotate the standardized variables by a prefix ``\texttt{Z.}''.

Further, the observation locations represent the electoral divisions as depicted in Figure~\ref{fig:01_GenEl2004} and are provided as Easting and Northing in meters. We transform them to kilometers to ensure computational stability while staying interpretable with respect to the range parameter. 

\subsection{Models and Methodologies}\label{sec:app_model}

We use two models in our comparison. First, a simple linear regression using the adaptive LASSO for variable selection is defined as
\begin{equation*}
\begin{aligned}
	y_i &= \texttt{Z.GenEl2004}_i \\
	&=\quad \phantom{\mu_1}
	&\phantom{+}\ &\mu_2 \texttt{Z.DiffAdd}_i 
	&+\ &\mu_3 \texttt{Z.LARent}_i \\
	&\quad+ \mu_4 \texttt{Z.SC1}_i 
	&+\ &\mu_5 \texttt{Z.Unempl}_i 
	&+\ &\mu_6 \texttt{Z.LowEduc}_i \\
	&\quad+ \mu_7 \texttt{Z.Age18\_24}_i 
	&+\ &\mu_8 \texttt{Z.Age25\_44}_i 
	&+\ &\mu_9 \texttt{Z.Age45\_64}_i + \varepsilon_i.
\end{aligned}
\end{equation*}


Since we use the standardized covariate $\texttt{Z.GenEl2004}$ as our response, we do not expect an intercept in the model. The coefficients are denoted as $\mu_j$ in accordance with our previous notation, i.e., these are only mean effects, and \textsf{ALASSO} denotes the adaptive LASSO used to estimate the mean effects. It is implemented using the \textsf{R} package \pkg{glmnet} \citep{Friedman2010}, where the corresponding adaptive weights are given by an ordinary least squares estimate and the shrinkage parameter is estimated by cross-validation. The second model is a full SVC model including an intercept:
\begin{equation*}
\begin{aligned}
	y_i &= \texttt{Z.GenEl2004}_i \\
	&=\quad \beta_1(s_i) 
	&+\ &\beta_2(s_i) \texttt{Z.DiffAdd}_i 
	&+\ &\beta_3(s_i) \texttt{Z.LARent}_i \\
	&\quad+ \beta_4(s_i) \texttt{Z.SC1}_i 
	&+\ &\beta_5(s_i) \texttt{Z.Unempl}_i 
	&+\ &\beta_6(s_i) \texttt{Z.LowEduc}_i \\
	&\quad+ \beta_7(s_i) \texttt{Z.Age18\_24}_i 
	&+\ &\beta_8(s_i) \texttt{Z.Age25\_44}_i 
	&+\ &\beta_9(s_i) \texttt{Z.Age45\_64}_i + \varepsilon_i.
\end{aligned}
\end{equation*}
The SVCs have yet to be specified according to the method being used to estimate the model, but generally, they do contain mean effects, i.e., the SVCs are not centered around zero. As for the intercept, we expect a zero mean effect. However, there might be some spatially local structures that can be captured using an SVC.

In the case of the GP-based SVC model as given in \eqref{eq:SVCmodelsingle}, the $\beta_j(s_i)$ can simply be partitioned into the mean effect $\mu_j$ and the random effect $\eta_j(s_i)$. For the GWR, $\beta_j(s_i)$ is defined as the generalized least square estimate where observations are geographically weighted. As a kernel to weight the observations based on their distances, we are using an exponential function. The kernel relies on an adaptive bandwidth that is selected using a corrected AIC \citep{Hurvich1998}.

We use MLE as well as PMLE to estimate the full SVC model, again labeled \textsf{MLE} and \textsf{PMLE}, respectively, and implemented using \pkg{varycoef}. For further reference, we also report the results of an GWR-based variable selection labeled \textsf{GWR} and using the \textsf{R} package \pkg{GWmodel} as well as an adaptive LASSO labeled \textsf{ALASSO} and using the \textsf{R} package \pkg{glmnet}.

\subsection{Results}\label{sec:DubVoter_results}

In this section, we present the results for all model-based approaches, i.e., \textsf{ALASSO}, \textsf{MLE}, and \textsf{PMLE}, as not all comparison measures of \textsf{GWR} are defined. In terms of variable selection, we provide the estimated shrinkage parameters, number of non-zero estimates of the mean and the variances, the log likelihood $\ell$ and the BIC in Table~\ref{tab:DubVoter_BIC}. As one can see, the \textsf{ALASSO} gives a very sparse model with a relative small log likelihood. As expected, \textsf{MLE} does not provide any sparse fixed effects but selects only 6 of the potentially 9 random effects. With \textsf{PMLE} we obtain a sparser model. Over all, the smallest BIC is achieved by \textsf{PMLE} followed by \textsf{MLE} and \textsf{ALASSO}.
 
\input{../application/output/table_DubVoter_BIC.tex} 
 
In Table~\ref{tab:DubVoter_parest}, the estimated parameters are provided. For the fixed effects, we observe that \textsf{PMLE} results in zero estimates for the intercept and \texttt{Z.LowEduc}. These fixed effects are only a subset of what \textsf{ALASSO} estimated to be zero.

\input{../application/output/table_DubVoter_parest.tex} 

In the covariance effects, we observe an interesting behavior. We note that \textsf{MLE} gives zero estimates for one third of the SVC variances even without any penalization. This coincides with previous results from the simulation study. As expected, the non-zero variance covariates of \textsf{PMLE} are a subset of the ones of \textsf{MLE}. Further details on the CD algorithm for \textsf{PMLE} are given in Appendix~\ref{app:CD_iter}.

Overall, the PMLE method results in less zero fixed effects compared to the adaptive Lasso. There is only one variable which does not enter the model at all, namely  \texttt{Z.LowEduc}, and only one other that does have a zero mean effect, namely the intercept. However, the linear model does not take the spatial structures into account. Considering the spatial structure apparently triggers the inclusion of additional fixed effects. The model gains complexity but we have to consider that this might be necessary in order to account for the spatial structures within the data. Therefore, SVC model selection remains a trade-off between model complexity and goodness of fit which we will address in the following section using cross-validation.


\subsection{Cross-Validation}

In the last section we examined the goodness of fit combined with model complexity as well as parameter estimation. We now turn to predictive performance. Here, we expect that due to high degree of flexibility of full SVC models, methods without any kind of variable selection could result in over-fitting on the training data in connection with biased spatial extrapolation on the prediction data set.

To examine this, we conduct a classical 10-fold cross-validation. That is, we randomly divide the Dublin Voter data set into ten folds of size 32 or 33, i.e., each observation $i$ falls into one of the following disjoint sets $\mathcal{S}_1, ..., \mathcal{S}_{10}: \bigcup_{\iota = 1}^{10} \mathcal{S}_\iota = \{ 1, ..., 322\}$. For each $\iota = 1, ..., 10$, the variable selection and model fitting of all four methods $m \in \{ \textsf{ALASSO}, \textsf{GWR}, \textsf{MLE}, \textsf{PMLE} \}$ is conducted on the union of nine sets (short hand notation $\mathcal{S}_{-\iota}$) with one set left out for validation ($\mathcal{S}_\iota$). We report the out-of-sample rooted mean squared errors (RMSE) on $\mathcal{S}_\iota$ for each method $m$ denoted $\textnormal{RMSE}_\iota(m)$. This procedure is repeated for each $\iota$. The results are visualized in Figure~\ref{fig:CV_bubble} and Table~\ref{tab:RMSE_summary}. 

\begin{figure}
	\centering
	\includegraphics[width=\textwidth]{../application/output/fig_03.png}
	\caption{Bubble plot visualizing the results of the cross-validation. The location of the bubbles corresponds to number of fixed and random effects selected by model. Note that the locations are slightly jittered to ensure overlapping methods are still visible. The size of the bubbles corresponds to the out-of-sample RMSE. The facets show individual results per fold.}\label{fig:CV_bubble}
\end{figure}

First, we observe that the size of the bubbles, i.e., the RMSE, varies more between different folds than between different methods. To analyze the differences in predictive performance, we provide the mean and standard deviation per method in Table~\ref{tab:RMSE_summary}. While the predictive performance of the estimated SVC models are similar, \textsf{ALASSO} falls behind. 

\input{../application/output/table_RMSE_summary.tex} 

Second, we address the number of selected fixed and random effects. Upon closer inspection of Figure~\ref{fig:CV_bubble}, we see notable variations of the number of selected fixed and random effects over different folds for all methods but \textsf{ALASSO}. We attribute this to the relatively small sample size. As already observed in Section~\ref{sec:DubVoter_results}, the number of selected mean effects is higher for selection methods PMLE and GWR on full SVC models than the adaptive LASSO on a linear model. However, the inclusion of more fixed effects and accounting the spatial structure using SVCs clearly surpasses the predictive performance of a simple linear model.
