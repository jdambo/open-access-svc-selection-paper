\appendix

\section{Appendix to: \protect\input{title_text}}

\subsection{Proofs}

\begin{proof}[Proof of Proposition~\ref{prop:gr0}]\label{proof:gr0}
	Let $U_A \in \mathbb{R}^{n\times n}$ be the distance matrix under some anisotropic geometric norm defined by a positive-definite matrix $A\in \mathbb{R}^{d\times d}$, i.e., $\left(U_A \right)_{lm} = u_{lm} := \|s_l - s_m\|_A$. Let $r: \mathbb{R}_{\geq 0} \rightarrow \left[0, 1\right]$ be a correlation function where by $r(U_A)$ we denote the component-wise evaluation, i.e., $\left(r(U_A)\right)_{lm} = r(u_{lm})$. We have:
	\begin{align*}
		\frac{\partial}{\partial \rho_\kappa} \bSigma_\Y(\btheta)  &= \frac{\partial}{\partial \rho_\kappa}\left( \sum_{k = 1}^q \left(\w^{(k)}{\w^{(k)}}^\top\right) \odot \bSigma_k + \tau^2 \I_{n\times n} \right) \\
			&= \left(\w^{(k)}{\w^{(k)}}^\top\right) \odot \frac{\partial}{\partial \rho_\kappa} \bSigma_\kappa = \\
			&= \left(\w^{(k)}{\w^{(k)}}^\top\right) \odot \sigma_\kappa^2 r'\left(\frac{U_A}{\rho_\kappa}\right)\odot\left(-\frac{U_A}{\rho_\kappa^2}\right) .
	\end{align*}
	With $|r'|<C$, we have $\frac{\partial}{\partial \rho_\kappa} \bSigma_\Y(\b_\kappa) = \0_{n\times n}$ and recall that $\bSigma_\Y(\b_\kappa)$ is well-defined and invertible. With the identities $(\log \det M)' = \textnormal{tr} (M^{-1} M')$ and $\left(M^{-1}\right)' = -M^{-1} M' M^{-1}$ for some quadratic matrix $M$, we obtain 
\begin{align*}
	\frac{\partial}{\partial \rho_\kappa} f (\btheta) & = \textnormal{tr} \left(  {\bSigma_\Y(\btheta)}^{-1} \frac{\partial}{\partial \rho_\kappa} \bSigma_\Y(\btheta) \right)  \\ &+ (\y-\X\bmu^{(t+1)})^\top \left( - {\bSigma_\Y(\btheta) }^{-1} \left(\frac{\partial}{\partial \rho_\kappa} \bSigma_\Y(\btheta) \right) {\bSigma_\Y(\btheta) }^{-1} \right) (\y-\X\bmu^{(t+1)})
\end{align*}	
and therefore $\frac{\partial}{\partial \rho_\kappa} f(\b_\kappa) = 0$.
	
\end{proof}

\subsection{Perturbed Grid}\label{app:per_grid}

A perturbed grid is used to sample the observation locations. It consists of $15 \times 15$ sampling domains arranged as a regular grid. Each sampling domain is a square surrounded by a thin margin. We sample an observation location uniformly from each square in the sampling domains. The true effect of the SVC is then sampled from an GP at corresponding observation locations. In Figure~\ref{fig:per_grid} such an example is provided. This is repeated for all $N = 100$ simulation runs.

\begin{figure}
\begin{center}
\includegraphics[width = 0.9\textwidth]{../simulation/output/fig_A1.png}
\caption{Perturbed grid of size $15 \times 15$ within the unit square (black border). The grey squares indicate the sampling domian. The colored points are the observation locations with corresponding value of the SVC, i.e., $\bfeta(\s)$.}\label{fig:per_grid}
\end{center}
\end{figure}

\subsection{Numeric Optimization}\label{app:num_opt}

We provide further details for the numeric optimization. In all ML and PML methods, we use the \R\ package \pkg{optimParallel} \citep{FG2019}. In the simulation study, recall that we are on a $m \times m$ perturbed grid, where we have $m = 15$ observations along one side. Here, we set $\sigma_k^2 \geq 0, \rho_k \geq (3m)^{-1}$, and $\tau^2 \geq 10^{-4}$ as lower bounds. The lower bounds in the case of the ranges are motivated by the \emph{effective range} of an exponential GP. It is $3\rho$, where $\rho$ is the corresponding range of the GP. Since the smallest distances between neighbors on a perturbed grid are $1/m$ on average, it is not feasible to model GPs with ranges smaller than a third of that. This prevents individual SVCs to take on the role of a nugget effect.


\subsection{Coordinate Descent Iterations}\label{app:CD_iter}

We briefly discuss the coordinate descent in \textsf{PMLE} from Sections~\ref{sec:app_model} and \ref{sec:DubVoter_results}. For the two covariates \texttt{Z.DiffAdd} and \texttt{Z.SC1}, we give the covariance parameters for respective SVCs at individual steps $t$ in the coordinate descent in Figure~\ref{fig:CD_iter}. We have chosen these two covariates as both their variances have the largest absolute difference between the initial and final values. Additionally, one of the variances shrunk to 0 where the other did not. In total, we have $T = 5$ iterations of the CD algorithm and the individual covariance parameters $(\rho_k^{(t)}, {\sigma_k^2}^{(t)})$ for $t = 0, ..., T$ and $k = 2, 4$ are given by the blue dots with annotated iteration step $t$. Note that for both covariates the covariance parameters of the last 3 iteration steps are (almost) identical. The lower bound of the variance is indicated by the dark green line. Figure~\ref{fig:CD_iter} clearly visualizes the effect of adding the penalties on the variance parameters as both optimization trajectories first evolve along the variance axis, before the range is adjusted further on.    For $\rho_4^{(t)}, t \geq 3$, we see that no further adjustments are made beside the fact that $\rho_4$ is unidentifiable under $\sigma_4^2 = 0$ (Proposition~\ref{prop:gr0}).
\begin{figure}
	\centering
	\includegraphics[width=\textwidth]{../application/output/fig_02.png}
	\caption{Covariance parameters trajectories of two SVCs for covariates \texttt{Z.DiffAdd} and \texttt{Z.SC1} in coordinate descent.}\label{fig:CD_iter}
\end{figure}