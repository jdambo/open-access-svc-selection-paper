\section{Variable Selection for GP-based SVC Models} \label{sec:SVCmodel}

\subsection{GP-based SVC Models}

Let $n$ be the number of observations, let $p$ be the number of covariates $\x^{(j)} \in \mathbb{R}^n, j = 1, ...,p$, for a fixed effect, and let $q$ be the number of covariates $\w^{(k)} \in \mathbb{R}^n, k = 1, ..., q$, with random coefficients, i.e., spatially varying coefficients. The fixed and random effects covariates do not necessarily need to be identical. Each observation $i$ is associated with an observation location $s_i$ in domain $D \subset \mathbb{R}^d, d\geq 1$. Further, let $\y \in \mathbb{R}^n$ be the vector of observed responses and $\bvarepsilon \sim \mathcal{N}_n(\0_n, \tau^2 \I_{n\times n})$ the error term (also called nugget). Without loss of generality, we assume the random coefficients to have zero mean and define a GP-based SVC model as
\begin{align}\label{eq:SVCmodelsingle}
	y_i = \sum_{j = 1}^p \mu_j x^{(j)}_i + \sum_{k = 1}^q  \eta_k(s_i) w^{(k)}_i + \varepsilon_i.
\end{align}
Here, the $k$th SVC is defined by a zero-mean GP $\eta_k(\cdot) \sim \mathcal{GP}(\0, c(\cdot, \cdot; \btheta_k))$ with covariance function $c$ and covariance parameters ${\btheta_k := (\rho_k, \sigma_k^2)}$, i.e., each SVC is parameterized by a range parameter $\rho_k$ and variance $\sigma_k^2$. We assume additional parameters like the smoothness of the covariance function $c$ to be known and that we can write
\begin{align}\label{eq:cov}
	c(s_l, s_m; \btheta_k) = \sigma_k^2 r\left(\frac{\| s_l - s_m \|_{A}}{\rho_k} \right),
\end{align}
where $r: \left[0, \infty \right) \rightarrow [ 0, 1]$ is a correlation function, $\| \cdot \|_A$ is an anisotropic geometric norm defined by a positive-definite matrix  $A \in \mathbb{R}^{d \times d}$ \citep{Wackernagel1995, Schmidt2020}. Note that \eqref{eq:cov} covers most of commonly used covariance functions such as the Mat\'ern class or the generalized Wendland class. For instance, the Mat\'ern covariance function is of the form of \eqref{eq:cov} with correlation function
 \begin{align}\label{eq:matern}
		r \left( u \right) = \frac{2^{1- \nu} }{\Gamma(\nu)} \left(\sqrt{2\nu} u \right)^\nu K_\nu \left(\sqrt{2\nu} u \right),
\end{align} 
where $\nu \in \mathbb{R}^{+}$ is the \emph{smoothness} parameter, $u = \| s_l - s_m \|_{A} / \rho$ is a scaled anisotropic distance, and $K_\nu$ is the modified Bessel function of the second kind and order $\nu$. For $\nu = 1/2$, equation~\eqref{eq:matern} reduces to the \emph{exponential function} $r(u) = \exp(-u)$ and the corresponding covariance function is therefore given by $c (s_l, s_m; \rho, \sigma^2 ) = \sigma^2 \exp(-\| s_l - s_m \|_{A}/\rho)$. Finally, we assume mutual prior independence between the SVCs $\eta_k(\cdot)$ as well as the nugget $\bvarepsilon$. 


For the observed data, the GPs $\bfeta_k(\cdot)$ reduce to finite dimensional normal distributions with covariance matrices $\bSigma_k$ defined as $\left( \bSigma_k \right)_{l, m} := c(s_l, s_m; \btheta_k)$. Thus, we have $\bfeta_k(\s) \sim \mathcal{N}_n(\0_{n}, \bSigma_k)$ and under the assumption of mutual prior independence the joint effect is given by $\bfeta(\s)  \sim \mathcal{N}_{nq}(\0_{nq}, \bSigma_\bfeta)$ with $\bSigma_\bfeta := \diag (\bSigma_1, ..., \bSigma_q)\in \mathbb{R}^{nq \times nq}$. We denote two data matrices as $\X$ and $\W$, where we have $\X \in \mathbb{R}^{n \times p}$ such that the $j$th column is equal to $\x^{(j)}$ and $\W := \left( \diag (\w^{(1)}), ..., \diag (\w^{(q)}) \right) \in \mathbb{R}^{n \times nq}$. Let $\bmu := (\mu_1, ..., \mu_p)^\top$ and $\btheta := (\rho_1, \sigma_1^2, ..., \rho_q, \sigma_q^2, \tau^2)^\top$ be the unknown vectors of fixed effects and covariance parameters. Then the GP-based SVC model is given by
\begin{align}\label{eq:SVCmodel}
	\y = \X\bmu + \W\bfeta(\s) + \bvarepsilon
\end{align}
and is parametrized by
\begin{align}\label{eq:omega}
	\bomega := (\bmu^\top,\btheta^\top )^\top \in \Omega := \mathbb{R}^p \times \left(\mathbb{R}_{>0} \times \mathbb{R}_{\geq 0} \right)^q \times \mathbb{R}_{> 0} .
\end{align}
The response variable $\Y$ follows a multivariate normal distribution $\Y \sim \mathcal{N}_n\left( \X \bmu, \bSigma_\Y(\btheta) \right)$ with covariance matrix
\begin{align}
	\bSigma_\Y(\btheta) := \sum_{k = 1}^q \left(\w^{(k)} {\w^{(k)}}^{\top} \right) \odot \bSigma_k) + \tau^2 \I_{n\times n}
\end{align}
and has the following log-likelihood
\begin{align}\label{eq:LL}
	\ell(\bomega) = -\frac{1}{2} \left( n\log (2\pi) + 
		\log \det \bSigma_{\Y} (\btheta) + \left(\y - \X \bmu \right)^\top \bSigma_{\Y}(\btheta)^{-1}\left(\y - \X \bmu \right) \right).
\end{align}
\citet{JAD2020} provide a computationally efficient MLE approach to estimate $\bomega$ by maximizing \eqref{eq:LL}, which we denote as $\hat{\bomega}(\MLE) := \argmax_{\bomega \in \Omega} \ell(\bomega)$.


\subsection{Penalized Likelihood}

We introduce a penalized likelihood that will induce global variable selection for the GP-based SVC model \eqref{eq:SVCmodel}. Related to this, we say that for $\mu_j \neq 0$ an effect of $\x^{(j)}$ and for $\bfeta_k(\s) \neq \0_n$ an effect of $\w^{(k)}$ on the response is given.  Note that variable selection for the random coefficients corresponds to choosing between $\sigma_k^2>0$ and $\sigma_k^2=0$. For the special case that $\x^{(j)} = \w^{(j)}$ for some $j$, there are 3 possible cases for each covariate to enter a model \citep{Reich2010}:
\begin{enumerate}
	\item The $j$th covariate is associated with a non-zero mean SVC, i.e., there exist a non-zero fixed effect and a random coefficient. 
	\item There only exists a fixed effect $\mu_j \neq 0$ for the $j$th covariate, but $\bfeta_j(\s)$ is identical to 0. 
	\item The $j$th covariate enters the model solely through the zero-mean SVC, i.e., $\mu_j = 0$ and $\bfeta_j(\s)$ not identical to 0.
\end{enumerate}
The parameters within $\bomega$ that we penalize are therefore $\mu_j$ and $\sigma_k^2$. Given some penalty function $p(|\cdot |)$, we define ${p_\lambda(|\cdot|) := \lambda p(|\cdot|)}$, where $\lambda$ acts as a shrinkage parameter. We augment the likelihood function \eqref{eq:LL} with penalties for the mean and variance parameters. In general, each of the parameters $\mu_j$ and $\sigma_k^2$ have their corresponding shrinkage parameter $\lambda_{j}>0$ and $\lambda_{p+k}>0$, respectively, yielding the \emph{penalized log-likelihood}:
\begin{align}\label{eq:pLL}
	p\ell(\bomega) = \ell(\bomega) - n\sum_{j = 1}^{p} p_{\lambda_j}(|\mu_j|) - n\sum_{k = 1}^{q} p_{\lambda_{p+k}}(|\sigma_k^2|).
\end{align}
For a given set of shrinkage parameters $\lambda_j, 1 \leq j \leq p + q$, we maximize the penalized likelihood function to obtain a \emph{penalized maximum likelihood estimate}:
\begin{align}\label{eq:PMLE}
	\hat{\bomega}(\PMLE) := \argmax_{\bomega \in \Omega} p\ell(\bomega ).
\end{align}
Note the similarity to \citet{Bondell2010} and \citet{Ibrahim2011} who present a joint variable selection by individually penalizing the fixed and random effects in linear mixed effects models. \citet{Mueller2013} give an overview of such selection and shrinkage methods.

Finally, one usually assumes the penalty function to be singular at the origin and non-concave on $(0, \infty)$ to ensure that $\hat{\bomega}(\PMLE)$ has favorable properties such as sparsity, continuity, and unbiasedness \citep{FanLi2001}. From now on, we will consider the $L_1$ penalty function, i.e., $p(| \cdot |) = | \cdot |$ \citep{Tibshirani1996}. 
