\section{Penalized Likelihood Optimization and Choice of Shrinkage Parameter}\label{sec:impl}

In this section, we show how to find the maximizer of the penalized log-likelihood and how we choose the tuning parameters $\lambda_j$. 


\subsection{Optimization of Penalized Likelihood}

In the following, we show how a maximizer of the penalized log-likelihood \eqref{eq:pLL} can be found for given $\lambda_j$. This is equivalent to minimizing the negative penalized log-likelihood. We propose a (block) coordinate descent (CD) which cyclically optimizes over the mean parameters $\bmu$ and covariance parameters $\btheta$, i.e., 
\begin{align}
	\bmu^{(t+1)} &:= \argmin_{\bmu\in \mathbb{R}^p} \left[ -p\ell(\bmu| \btheta^{(t)}) \right], \label{eq:CDAmu} \\
	\btheta^{(t+1)} &:= \argmin_{\btheta \in \Theta} \left[ -p\ell(\btheta | \bmu^{(t+1)}) \right], \label{eq:CDAtheta}
\end{align}
for $t \geq 0$. The initial values are given by the ML estimate $\btheta^{(0)}:=\hat{\btheta}(\MLE)$. Algorithm~\ref{alg:CDA} summarizes this block coordinate descent approach. In the following, we show how the component wise minimizations \eqref{eq:CDAmu} and \eqref{eq:CDAtheta} are realized. 

\begin{algorithm}
	\KwData{shrinkage parameters $\lambda_j$, convergence threshold $\delta$, ML estimate $\hat{\btheta}(\MLE)$, objective function $p\ell(\cdot)$}
 	\KwResult{$\hat{\bomega}(\PMLE)$}
 	Initialize $t \leftarrow 0, \btheta^{(0)} \leftarrow \hat{\btheta}(\MLE)$ \;
 	\Repeat{$\|\btheta^{(t)}-\btheta^{(t-1)}\|_1 / \|\btheta^{(t-1)}\|_1 < \delta$}{
  		$\bmu^{(t+1)} \leftarrow \argmin_{\bmu \in \mathbb{R}^p} \left[ -p\ell(\bmu|  \btheta^{(t)}) \right]$\; \label{alg:CDAeq1}
  		$\btheta^{(t+1)} \leftarrow \argmin_{\btheta\in \Theta}  \left[ -p\ell(\btheta| \bmu^{(t+1)}) \right]$\;\label{alg:CDAeq2}
  		$t \leftarrow t + 1$ \;
  	}
  	$\hat{\bomega}(\PMLE) \leftarrow \bigl( {\btheta^{(t)}}^\top, {\bmu^{(t)}}^\top \bigr)^\top$\;
 \caption{General coordinate descent algorithm for penalized likelihood.}\label{alg:CDA}
\end{algorithm}


\subsubsection{Optimization over Mean Parameters}

We fix the covariance parameters $\btheta^{(t)}$ for some $t \geq 0$. The optimization step for the mean parameter (line~\ref{alg:CDAeq1}, Algorithm~\ref{alg:CDA}) simplifies to 
\begin{align*}
	\bmu^{(t+1)} &= \argmin_{\bmu \in \mathbb{R}^p} \left[ -\ell(\bmu | \btheta^{(t)}) + n\sum_{j = 1}^{p} \lambda_j |\mu_j| \right]\\
	&= \argmin_{\bmu \in \mathbb{R}^p} \left[ \frac{1}{2}\left(\y - \X \bmu \right)^\top \bSigma_{\Y}(\btheta^{(t)})^{-1}\left(\y - \X \bmu \right) + n\sum_{j = 1}^{p} \lambda_j |\mu_j| \right]
\end{align*}
We note that the first term is a generalized least square estimate for a linear model with $\Y \sim\mathcal{N}_n \left(\X\bmu, \bSigma_{\Y}(\btheta^{(t)})\right)$. Using the Cholesky decomposition $\L$ of $\bSigma_{\Y}(\btheta^{(t)})$ and a simple variable transformation $\tilde{\y} := \L^{-1} \y$ and $\tilde{\X} := \L^{-1} \X$, the objective function simplifies to
\begin{align*}
	\bmu^{(t)} = \argmin_{\bmu \in \mathbb{R}^p} \left[ \frac{1}{2n}\|\tilde{\y} - \tilde{\X}\bmu \|_2^2 + \sum_{j = 1}^{p} \lambda_j |\mu_j| \right].
\end{align*}
We note that this objective function coincides with a LASSO for a classical linear regression model \citep{Tibshirani1996} with individual shrinkage parameters per coefficient.

\subsubsection{Optimization over Covariance Parameters}

We optimize over the covariance parameters $\btheta$ with fixed mean parameter $\bmu^{(t+1)}$ (c.f. Algorithm~\ref{alg:CDA}, line~\ref{alg:CDAeq2}). We rearrange this optimization problem writing $\btheta^{(t+1)} = \argmin_{\btheta\in \Theta} f(\btheta)$ with the following objective function:
\begin{align} \label{eq:objfun}
	f (\btheta) := -\ell \left( \bigl({\bmu^{(t)}}^\top, \btheta^\top \bigr)^\top \right) +n \sum_{k = 1}^{q} \lambda_{p + k} |\sigma^2_k|.
\end{align}
We use numeric optimization to obtain $\btheta^{(t+1)}$. In particular, we use a quasi Newton method \citep{Byrd1995} for the numeric optimization. Note that for a $\kappa \in \{ 1, ..., q \}$ with $\sigma_\kappa^2 = 0$, the corresponding range $\rho_\kappa$ is not identifiable and might induce non-convergence issues. The following proposition ensures that in case described above the optimization is well behaved.

\begin{prop}\label{prop:gr0}
	Let $r$ be a correlation function for a GP-based SVC model as given above. Assume that the derivative of $r$ exists and is bounded, i.e., $|r'|<C$ for some constant $C \geq 0$. Let $B_\kappa:= \{\btheta \in \Theta : \sigma_\kappa^2 = 0\}$ for $\kappa \in \{1, ..., k\}$. Then the objective function $f$ defined in \eqref{eq:objfun} fulfills
	\begin{align*}
		\frac{\partial}{\partial \rho_\kappa} f(\b_\kappa) = 0,
	\end{align*}
	for all $\b_\kappa \in B_\kappa$ and $\kappa \in \{1, ..., k\}$.
\end{prop}

\begin{proof}
	The proof is given in the Appendix~\ref{proof:gr0}.
\end{proof}

The additional assumption $|r'|<C$ is quite weak and fulfilled by most of covariance functions used for modeling GP. The partial derivative with respect to $\rho_\kappa$ is 0 and therefore the approximation of the gradient function for $\rho_\kappa$ yields values very close or identically to 0. Therefore, in the case of $\sigma_\kappa^2 = 0$, the numeric optimization will stop making any adjustments along the $\rho_\kappa$ direction. 

\subsection{Choice of Shrinkage Parameters}

We use the adaptive LASSO (ALASSO, \citealp{Zou2006}) for the parameters $\lambda_j$ given by
\begin{align}\label{eq:adappenals}
	\lambda_j := \frac{\lambda_\bmu}{|\hat{\mu}_j|}, \quad \quad \lambda_{p + k} := \frac{\lambda_\btheta}{|\hat{\sigma}^2_k|},
\end{align}
where we use the ML estimates $\hat{\mu}_j(\MLE)$ and $\hat{\sigma}^2_k(\MLE)$ to weight the shrinkage parameters $(\lambda_\bmu, \lambda_\btheta) \in \Lambda := \left(\mathbb{R}_{>0}\right)^2$. These two parameters account for the differences between the mean and variance parameters. This leaves us with the task to find a sensible choice of shrinkage parameters $\blambda := (\lambda_\bmu, \lambda_\btheta) \in \Lambda$. We choose these parameters by maximizing an information criterion (IC) which combines the goodness of fit (GoF) and model complexity (MC), i.e, 
\begin{align*}
	\textnormal{IC} = \textnormal{GoF} + \textnormal{MC}.
\end{align*}
An alternative approach that is computationally more expensive is to use cross-validation. To emphasize the dependency on $\blambda$, we introduce the short hand notation $\hat{\bomega}_\blambda$ for denoting the PML estimate of $\bomega$ for a given $\blambda$. The corresponding mean parameters and variances are given by $\hat{\bmu}_\blambda$ and $\hat{\bsigma}^2_\blambda$, respectively. We use a BIC type information criterion which is given by
\begin{align}\label{eq:BIC}
	\BIC = -2\ell(\hat{\bomega}_\blambda) + \log(n)\left(\|\hat{\bmu}_\blambda \|_0 + \| \hat{\bsigma}^2_\blambda\|_0 \right),
\end{align}
where $\| \cdot \|_0$ is the count of non-zero entries. That is, the MC is captured via the number of non-zero fixed effects and non-constant random coefficients. Of course, there exist various ICs like, for instance, the corrected Akaike IC (cAIC, see \citealp{Mueller2013}, for an overview). In our empirical experience the BIC performs best with respect to variable selection which is in line with \citet{Schelldorfer2011}.


Our goal is to find a minimizer $\blambda$, i.e., ${\hat{\blambda}_\BIC := \argmin_{\blambda \in \Lambda}\BIC(\blambda)}$. A single evaluation of $\BIC(\blambda)$ is computationally expensive, which is why we want the number of evaluations as low as possible. To this end, we use model-based optimization (MBO, also known as Bayesian optimization, \citealp{Jones2001, Koch2012, Horn2016}). For our objective function $\BIC(\blambda)$, we initialize the optimization by drawing a Latin square sample of size $n_{init}$ from $\Lambda$ which we define as $\{ \blambda^{(1)}, ..., \blambda^{(n_{init})} \}  \subset \Lambda$. Then we compute $\xi^{(i)} = \BIC(\blambda^{(i)})$ to obtain the tuples $\bigl(\xi^{(i)}, \blambda^{(i)}\bigr)$. Now we can fit a surrogate model to the tuples. In our case, we assume a kriging model defined as a Gaussian process with a Mat\'ern covariance function of smoothness $\nu = 3/2$, c.f. equation~\eqref{eq:matern}. That is, given the observed tuples, we have $\Xi(\blambda)\sim \mathcal{N} \left(\hat{\mu}(\blambda), \hat{s}^2(\blambda) \right)$ as our surrogate model. An infill criterion is used to find the next, most promising $\blambda^{(n_{init} + 1)}$. We use the expected improvement (EI) infill criterion which is derived from the current surrogate model $\Xi(\blambda)$. The EI at $\blambda$ is defined as
\begin{align}\label{eq:EI}
	\textnormal{EI}(\blambda) = E_{\Xi}\left(\max \{ \xi_{min} - \Xi(\blambda), 0 \} \right),
\end{align}
where $\xi_{min}$ denotes the current BIC minimum. In the case of a GP-based surrogate model, the EI \eqref{eq:EI} can be expressed analytically. A separate optimization of \eqref{eq:EI} returns the best parameter $\blambda^{(n_{init} + 1)}$ according to the infill criteria and then the BIC is calculated. The tuple $\left(\xi^{(n_{init} + 1)}, \blambda^{(n_{init} + 1)}\right)$ is added to the existing tuples and surrogate model is updated. This procedure is repeated $n_{iter}$ times. Finally, the minimizer of the BIC from the set $\{\blambda^{(i)} : i = 1, ..., n_{init} + n_{iter}\}$ is returned. For more details regarding MBO refer to \citet{R:mlrMBO}. 


\subsection{Software Implementation}

The described \PMLE\ approach is implemented in the \R\ package \pkg{varycoef} \citep{R:varycoef}. It contains the discussed MBO which is implemented using the \R\ package \pkg{mlrMBO} \citep{R:mlrMBO} as well as a grid search to find the best shrinkage parameters. Further, the \pkg{varycoef} package implements our proposed optimization of the penalized likelihood using a coordinate descent. The optimization over the mean parameters is executed using a classical adaptive LASSO with the \R\ package \pkg{glmnet} \citep{Friedman2010}. For the optimization over the covariance parameters, we use the quasi Newton method \texttt{"L-BFGS-B"} \citep{Byrd1995} which is available in a parallel version in the \R\ package \pkg{optimParallel} \citep{FG2019}.