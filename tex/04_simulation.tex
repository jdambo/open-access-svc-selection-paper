\section{Simulation}\label{sec:simu}

\subsection{Set-Up}

We simulate $N = 100$ data sets consisting of $n = 15^2 = 225$ samples with observation locations $\s_i, i = 1, ..., n,$ from a $15 \times 15$ \emph{perturbed grid}, c.f. Appendix~\ref{app:per_grid} and \citet{Furrer2016, JAD2020}. As in \citet{Tibshirani1996} and his suggested simulation set up, which has been assumed (e.g.~\citealp{FanLi2001}) or adapted (e.g.~\citealp{Li2008, Ibrahim2011}) in other simulation studies for variable selection, we use $p=q=8$ covariates which are sampled from a multivariate zero mean normal distribution such that the covariance between $\x^{(j)}$ and $\x^{(k)}$ is $\gamma^{|j-k|}$ with $\gamma = 0.5$. For each simulation run, we sample the covariates $\x^{(j)}$ which are both associated with a fixed and random effect. Let $\X$ and $\W$ be the corresponding data matrices as defined above. Concerning the fixed effects parameters, we assume
\begin{align}\label{eq:true_mu}
	\bmu = (3, 1.5, 0, 0, 2, 0, 1, 0)^\top,
\end{align}
in order to obtain a balanced design in the sense that for each possible combination of zero and non-zero parameters of both the fixed and random effects there are two cases. Let $\bfeta_k(\s) \sim \mathcal{N}_n(\0_n, \bSigma_k)$ for $k = 1, ..., q$ be the GP-based SVCs defined by an exponential covariance function with corresponding parameters provided in Table~\ref{tab:simcovpars}. For $k\in \{2, 4, 7, 8\}$ the respective variance $\sigma_k^2$ is zero and therefore the respective true SVCs $\bfeta_k(\s)$ are constant zero. Hence, the true model contains two of each covariates with a non-zero mean SVC ($k = 1, 5$), constant non-zero mean effects ($k = 2, 7$), zero mean SVC ($k = 3, 6$), and without any effect ($k = 4, 8$). Adding a sampled nugget effect $\bvarepsilon \sim \mathcal{N}_n(\0_n, \tau^2\I_{n\times n})$ with nugget variance $\tau^2 = 0.1$ and independent of the GPs we can compute the response $\y$.


We compare methodologies for estimating the SVC model, c.f. \eqref{eq:SVCmodel}, 
or the oracle SVC model
\begin{align}\label{eq:oracleSVC}
	\y &= \X  (\mu_1, \mu_2, 0, 0, 0, \mu_5, 0, \mu_7, 0)^\top + \sum_{k \in  \{1, 3, 5, 6\}} \bfeta_k(\s) \odot \x^{(k)} + \bvarepsilon.
\end{align}
The latter one is called the oracle model as it assumes the true data generating covariates to be known and excludes all other parameters from their respective estimation. As a reference, we will use two classical MLE approaches without any shrinkage or variable selection to estimate \eqref{eq:SVCmodel} and \eqref{eq:oracleSVC} labeled \textsf{MLE} and \textsf{Oracle}, respectively. The third methodology is our novel approach which estimates \eqref{eq:SVCmodel} and we denote it by \textsf{PMLE}. For the coordinate descent algorithm, we set a relative convergence threshold of $\delta = 10^{-6}$ and a maximum of $T_{max} = 20$ iterations. Note that this upper limit on the number of iterations was never attained in our simulations. Further, the range of shrinkage parameters was set to $(10^{-6}, 1)$, i.e., $\blambda = (\lambda_\bmu, \lambda_\btheta)^\top \in (10^{-6}, 1) \times (10^{-6}, 1)$. Finally, the shrinkage parameters $\hat{\blambda}_{\textnormal{BIC}}$ are estimated by MBO using $n_{init} = 10$ initial evaluations and $n_{iter} = 10$ iteration steps. The infill criterion is the previously defined expected improvement given in equation \eqref{eq:EI}. The shrinkage parameter ranges as well as the number of initial evaluations and iteration steps was chosen such that we obtain reasonable results under a feasible time and computational resource constraint. The methods \textsf{PMLE}, \textsf{MLE}, and \textsf{Oracle} are implemented using \pkg{varycoef} \citep{R:varycoef}. Further details are given in Appendix~\ref{app:num_opt}.



% summary statistics table of simulation study \label{tab:simcovpars}
\input{../simulation/output/table_truepar.tex}

\subsection{Results}

First, we check the convergence properties of the CD over all simulations. This is depicted in Figure~\ref{fig:CD_BIC}. Facet (a) shows a histogram of the number of evaluated iterations $T$, where the median over all iterations was $4$. This shows that the CD algorithm converges quickly. Second, the estimated shrinkage parameters are depicted in Figure~\ref{fig:CD_BIC}(b). We observe that there exist two regimes with respect to $\lambda_\btheta$. The borders of the shrinkage parameter space used in the MBO are depicted darkgreen. There are some estimates very close to the border. Upon closer inspection of actual parameter estimates (see below) we could not find any substantial difference between parameter estimates obtained by $\hat{\blambda}_{\textnormal{BIC}}$ close to the boundary compared to all other $\hat{\blambda}_{\textnormal{BIC}}$. 

\begin{figure}
\begin{center}
\includegraphics[width = \textwidth]{../simulation/output/fig_01.png}
\caption{The coordinate descent with (a) the number of iterations steps $T$ and (b) the MBO estimated $\hat{\blambda}$ under the BIC.} \label{fig:CD_BIC}
\end{center}
\end{figure}

Second, we turn to the actual parameter estimates. They are depicted in Figure~\ref{fig:par_est} and grouped by variances, ranges, and means. The true values are indicated by red lines. Further, the number of zero estimates is given, if there are any. As one can see, the \textsf{PMLE} returns sparse estimates in the fixed effects as well as the variances. We can clearly observe that for all methods, the estimation of the mean parameters is much more precise compared to the covariance parameters. However, the MLE method is not able to return mean parameters identical to zero. For the variances, we observe that both the \textsf{MLE} and \textsf{PMLE} are capable of obtaining sparse estimates. Here, the additional penalization of the variances manifests itself in the higher number of zero estimates. On average, we observe an increase of 25 correctly zero estimated variance parameters. The penalty has a minor downside as there are some zero estimates, where the true variance is unequal zero. In that case, the nugget compensates for unattributed variation by the covariates which manifests itself in large variance estimates, c.f.~outliers of box plot for estimated nugget variance, Figure~\ref{fig:par_est}.


\begin{figure}
\begin{center}
\includegraphics[width = \textwidth]{../simulation/output/fig_02.png}
\caption{Parameter estimates as box plots and, if any, number of zero estimates. Simulation setup: $N = 100$ simulation runs, $n = 225$ observations on a perturbed $15\times 15$ grid. Red line are the true values, c.f. \eqref{eq:true_mu} and Table~\ref{tab:simcovpars}.}\label{fig:par_est}
\end{center}
\end{figure}


Finally, we summarize the results of the simulation study in Table~\ref{tab:sumsim}. For each simulation and methodology $m$, we compute the relative model error (RME) which is defined as
\begin{align*}
	\textnormal{RME}(m) =  \frac{ \| \y - \hat{\y}(m) \|_1}{ \| \y - \bar{y} \cdot \1_n \|_1}.
\end{align*}
The median relative model error (MRME), i.e., the median over all RME, is provided in Table~\ref{tab:sumsim} and is smallest for \textsf{MLE}. This comes at no surprise given the high degree of flexibility of an SVC model. It shows that some kind of variable selection is desirable in order to counter over-fitting. The MRME for \textsf{PMLE} is similar to the one of \textsf{Oracle}. It is not identical as the parameter estimates cannot fully mimic the behavior of the oracle. This is summarized in the second part of Table~\ref{tab:sumsim}, where the number of estimated zero parameters are given both within the mean effects of $\bmu$ and the random effects of $\btheta$. The average number of correctly identified zero parameter (C) and incorrectly identified zero parameters (IC) over all simulations is provided. \textsf{PMLE} introduces sparse estimates for the fixed effects while there are no IC in the fixed effects. Further, \textsf{PMLE} substantially increases the C for the random effects. The downside is a slight increase in the IC. It is also worth mentioning that both the \textsf{MLE} and \textsf{PMLE} do not incorrectly estimate any mean parameter as zero. 

% summary statistics table of simulation study \label{tab:sumsim}
\input{../simulation/output/table_sumstat.tex}



\subsection{Discussion}

In this section, we summarize our results of the simulation study. Our first focus is the selection of covariance parameters, where the overall performance is not on par with ones of the mean parameters. However, this comes at no surprise as covariance parameters are known to be more difficult to estimate than mean parameters. Further, we can observe a familiar behavior from variable selection of classical linear models. Increasing the sparsity of the model, i.e., having none or only a few SVC, will increase the variance of the nugget effect. 

Overall, our newly suggested PMLE method correctly identifies covariates with no effect in over 90\% of the fixed and 85\% of the random effects. This is notable considering the only drawback is that less than 5\% of covariates were incorrectly estimated to have no effect. 

