## -- loading data -----

# Dublin Voter Data
data("DubVoter", package = "GWmodel")
# adding projection
proj4string(Dub.voter) <- "+proj=tmerc +lat_0=53.5 +lon_0=-8 +k=1.000035 +x_0=200000 +y_0=250000 +a=6377340.189 +b=6356034.447938534 +units=m +no_defs "
# make spatial object
locs <- as.data.frame(Dub.voter[, c("X", "Y")])
sp.locs <- locs
colnames(sp.locs) <- c("x", "y")
coordinates(sp.locs) <- ~x+y
proj4string(sp.locs) <- proj4string(Dub.voter)

# check 
str(sp.locs)

## -- export summary statistics of Dublin data -----
# table 3

sum.stat.DV <- Dub.voter@data %>%
  # drop IDs and coordinates
  select(-(1:3)) %>% 
  apply(., 2, function(x) c(
    min = min(x),
    mean = mean(x),
    sd = sd(x),
    max = max(x)
  )
  ) %>% t()

tab.out <- data.frame(
  var = paste0("\\texttt{", 
               str_replace_all(
                 rownames(sum.stat.DV), fixed("_"), "\\_"
               ), 
               "}"),
  descrip = c(
    "...who are one-year migrants", 
    "...who are local authority renters",
    "...who are social class one (high)", 
    "...who are unemployed",
    "...who are with little formal education", 
    "...who are age group",
    "...who are age group",
    "...who are age group",
    "...who voted in 2002 GE"
  ), 
  sum.stat.DV
)

colnames(tab.out) <- c(
  "", 
  "Percentage of population in each ED...",
  "Min.", "Mean", "SD", "Max."
)



addtorow <- list()
addtorow$pos <- list(-1)
addtorow$command <- 
  "\\hline \\textbf{Variable}&	\\textbf{Description}& \\multicolumn{4}{c}{\\textbf{Summary Statistics} [in \\%]} \\\\
	  		  \\cmidrule(lr){3-6}"

xtb <- xtable(
  tab.out, 
  latex.environments = "center", 
  floating = TRUE,
  align = c("l", "l", "l", rep("r", 4)),
  digits = c(1, 1, 1, rep(2, 4)),
  caption = "Description and summary statistics of Dublin Voter data. The descriptions are taken from the package \\pkg{GWmodel}. The summary statistics include the minimum, mean, standard deviation (SD), and maximum. Note that due to an naming error the response of interest is called \\texttt{GenEl2004}, although it actually refers to the 2002 GE.", 
  label = "tab:DubVoter_sumstat"
)

print(xtb, 
      file = "application/output/table_DubVoter_sumstat.tex",
      sanitize.text.function = function(x){x},
      add.to.row = addtorow,
      caption.placement = "top",
      hline.after = c(0, 9),
      NA.string = "--",
      include.rownames = FALSE)


# figure 1_Application

tm <- tm_shape(Dub.voter) + 
  # polygons of Voter Turnout
  tm_polygons(
    col = "GenEl2004", 
    palette = "viridis"
  ) + 
  # add observation locations
  tm_shape(sp.locs) +
  tm_dots(col = "black", size = 0.1) +
  # make it pretty
  tm_layout(legend.title.size = 1.4,
            legend.text.size = 0.9,
            legend.bg.color = "white",
            legend.bg.alpha = 0,
            legend.format = list(
              fun = function(x) paste0(formatC(x, digits=0, format="f"), "%")
            )
  ) +
  tm_scale_bar(text.size = 1.4,
               position = c("right", "BOTTOM")) + 
  tm_compass(position = c("left", "bottom")) 

tmap_save(tm, filename = paste0("application/output/fig_01.png"))


## -- Data Preparation -----

# standardizing
dat <- Dub.voter@data[, -(1:3)]
Z.dat <- as.data.frame(apply(dat, 2, scale))
# Z.dat$GenEl2004 <- dat$GenEl2004
colnames(Z.dat) <- paste0("Z.", colnames(dat))

n <- nrow(Z.dat)